#ifndef _KRAWEDZ_  
#define _KRAWEDZ_  

#include "wieszcholek.h"


template<typename E>
class krawedz
{
private:
	E wartosc;
	wieszcholek<E>* end[2];

public:

	krawedz(wieszcholek<E>* a=nullptr, wieszcholek<E>* b=nullptr, E w=1)
	{
		end[0] = a;
		end[1] = b;
		wartosc = w;
	}

	~krawedz() = default;
	
	void set_wartosc(E ele)
	{
		wartosc = ele;
	}

	E get_wartosc()
	{
		return wartosc;
	}

	void set_jeden(wieszcholek<E> *j)
	{
		end[0] = j;
	}

	wieszcholek<E> * get_jeden()
	{
		return end[0];
	}

	void set_drugi(wieszcholek<E> *d)
	{
		end[1] = d;
	}

	wieszcholek<E>* get_drugi()
	{
		return end[1];
	}

	wieszcholek<E>** get_tab()
	{
		return end;
	}
};

#endif 