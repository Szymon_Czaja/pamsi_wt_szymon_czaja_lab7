
#pragma once

#ifndef _TABLICA_HASZUJACA_POBKOWANIE_  
#define _TABLICA_HASZUJACA_POBKOWANIE_  
#include <iostream>
#include <string>
using namespace std;

class tablica_haszujaca_probkowanie
{
private:

	int  *tab;
	int wielkosc;
public:
	tablica_haszujaca_probkowanie(int w)
	{
		if (pierwsza(w))
			wielkosc = w;
		else
			wielkosc = 7919;
		tab = new int[wielkosc]();
	}

	~tablica_haszujaca_probkowanie()
	{
		delete[]tab;
	}


	int kompresja(int ele)
	{
		return ele%wielkosc;
	}

	void insert(int ele)
	{
		int index = kompresja(ele);
		while (!(tab[index] == 0|| tab[index] == -1)&&index<wielkosc)
			index++;
		if (index == wielkosc)
		{
			index = 0;
			while (!(tab[index] == 0 || tab[index] == -1) && index<tab[kompresja(ele)])
				index++;
		}
		if((tab[index] == 0 || tab[index] == -1))
			tab[index]=ele;
		else
		{
			string wyjatek = " tablica przepelniona !";
			throw wyjatek;
		}

		if (index >= kompresja(ele))
			cout << "liczba probek potrzebna do dodania: " << index - kompresja(ele) << endl;
		else
			cout << "liczba probek potrzebna do dodania: " << wielkosc - kompresja(ele) + index << endl;
	}

	void remove(int ele)
	{
		int index = kompresja(ele);
		while (!(tab[index] == ele || tab[index] == 0) && index<wielkosc)
			index++;
		if (index == wielkosc)
		{
			index = 0;
			while (!(tab[index] == ele || tab[index] == 0) && index<kompresja(ele))
				index++;
		}
		if (tab[index] == ele)
			tab[index] = -1;
		else if (tab[index] == 0||index== kompresja(ele))
			cout << "nie znalezono elementu do usuniecia! " << endl;
	

		if (index >= kompresja(ele))
			cout << "liczba probek potrzebna do usunieca: " << index - kompresja(ele) << endl;
		else
			cout << "liczba probek potrzebna do usunieca: " << wielkosc - kompresja(ele)+ index << endl;
	}

	void search(int ele)
	{
		int index = kompresja(ele);
		while (!(tab[index] == ele && tab[index] == 0) && index<wielkosc)
			index++;
		if (index == wielkosc)
		{
			index = 0;
			while (!(tab[index] == ele && tab[index] == 0) && index<kompresja(ele))
				index++;
		}

		if (tab[index] == ele)
			cout << "zaleziono element na pozycji: "<<index << endl;
		else if (tab[index] == 0|| index == kompresja(ele))
			cout << "nie znalezono elementu " << endl;
	

		if (index >= kompresja(ele))
			cout << "liczba probek potrzebna do znalezienia: " << index - kompresja(ele) << endl;
		else
			cout << "liczba probek potrzebna do znalezienia: " << wielkosc - kompresja(ele) + index << endl;
	}

	void print()
	{
		for (int i = 0;i < wielkosc;i++)
			if(tab[i]!=0&& tab[i]!=-1)
				cout<< tab[i] << endl;
	}

	bool pierwsza(int n)
	{
		if (n<2)
			return false;
		for (int i = 2;i*i <= n;i++)
			if (n%i == 0)
				return false;
		return true;
	}

};

#endif 