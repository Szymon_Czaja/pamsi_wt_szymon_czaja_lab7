
#include"tablica_haszujaca_link.h"
#include"tablica_haszujaca_probkowanie.h"
#include"tablica_haszujaca_podwojna.h"

#include <stdlib.h>     
#include <time.h> 

using namespace std;

int main()
{
	srand(time(NULL));

	try {
		int ran;
		Deque<int> list;
		tablica_haszujaca_podwojona po(23);
		tablica_haszujaca_link l(23);
		tablica_haszujaca_probkowanie pr(23);

		for (int i = 0;i < 10;i++)
		{
			ran = rand() % 1000 + 1;
			if (!list.jest(ran))
			{
				po.insert(ran);
				l.insert(ran);
				pr.insert(ran);
				list.add_back(ran);
			}
			else
				i--;
		}
	

	po.print();
	l.print();
	pr.print();
	
	}
	catch (string t)
	{
		cout << "znaleziono wyjatek! " << t << endl;
	}
	system("PAUSE");
	return 0;
}