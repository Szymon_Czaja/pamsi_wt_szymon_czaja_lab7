#pragma once

#ifndef _TABLICA_HASZUJACA_PODWOJONA_  
#define _TABLICA_HASZUJACA_PODWOJONA_  
#include <iostream>
#include <string>
using namespace std;

class tablica_haszujaca_podwojona
{
private:

	int  *tab;
	int wielkosc;
	int q;
public:
	tablica_haszujaca_podwojona(int w)
	{
		if (pierwsza(w))
			wielkosc = w;
		else
			wielkosc = 7919;
		int i = wielkosc-1;
		while (!pierwsza(i))
			i--;
		q = i;
		tab = new int[wielkosc]();
	}

	~tablica_haszujaca_podwojona()
	{
		delete[]tab;
	}


	int kompresja(int ele)
	{
		return ele%wielkosc;
	}

	int kompresja_2(int ele)
	{
		return q - ele%q;
	}

	void insert(int ele)
	{
		int index= kompresja(ele);
		if (tab[index] == 0 || tab[index] == -1)
		{
			tab[index] = ele;
			cout << "liczba probek potrzebna do dodania: 0" << endl;
			return;
		}

		for (int i = 0;i < wielkosc;i++)
			if (tab[index] == 0 || tab[index] == -1)
			{
				tab[index] = ele;
				cout << "liczba probek potrzebna do dodania: " << i << endl;
				return;
			}
			else
				index = (kompresja(ele) + i*kompresja_2(ele)) % wielkosc;

		if (index==wielkosc)
		{
			string wyjatek = " tablica przepelniona !";
			throw wyjatek;
		}

	}

	void remove(int ele)
	{
		int index= kompresja(ele);
		if (tab[index] == ele)
		{
			tab[index] = -1;
			cout << "liczba probek potrzebna do usuniecia: 0" << endl;
			return;
		}

		int i;
		for (i = 0;i < wielkosc;i++)
		{
			if (tab[index] == ele)
			{
				tab[index] = -1;
				cout << "liczba probek potrzebna do usuniecia: " << i << endl;
				return;
			}
			else
				index = (kompresja(ele) + i*kompresja_2(ele)) % wielkosc;

			if (tab[index] == 0)
			{
				cout << "nie znaleziono elementu do usuniecia " << endl;
				return;
			}
		}

		if (i == wielkosc)
			cout << "nie znaleziono elementu do usuniecia " << endl;

	}

	void search(int ele)
	{

		int index= kompresja(ele);
		if (tab[index] == ele)
		{
			cout << "potrzeba by�o 0 probek do znalezienia element na pozycji: " << index << endl;
			return;
		}

		int i;
		for ( i = 0;i < wielkosc;i++)
		{
			if (tab[index] == ele)
			{
				cout << "potrzeba by�o "<<i<<"probek do znalezienia element na pozycji: " << index << endl;
				return;
			}
			else
				index = (kompresja(ele) + i*kompresja_2(ele)) % wielkosc;
			
			if (tab[index] == 0)
			{
				cout << "nie znaleziono elementu " << endl;
				return;
			}
		}

		if (i == wielkosc)
			cout << "nie znaleziono elementu " << endl;
	}

	void print()
	{
		for (int i = 0;i < wielkosc;i++)
			if (tab[i] != 0 && tab[i] != -1)
				cout << tab[i] << endl;
	}

	bool pierwsza(int n)
	{
		if (n<2)
			return false;
		for (int i = 2;i*i <= n;i++)
			if (n%i == 0)
				return false;
		return true;
	}

};

#endif 