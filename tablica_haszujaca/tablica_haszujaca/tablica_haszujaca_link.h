
#pragma once

#ifndef _TABLICA_HASZUJACA_LINK_  
#define _TABLICA_HASZUJACA_LINK_  


#include "lista.h"




class tablica_haszujaca_link
{
private: 
	
	Deque<int> *tab;
	int wielkosc;
public:
	tablica_haszujaca_link(int w)
	{
		if (pierwsza(w))
			wielkosc = w;
		else
			wielkosc = 7919;
		tab = new Deque<int>[wielkosc];
	}

	~tablica_haszujaca_link()
	{
		delete[] tab;
	}

	int kompresja(int ele)
	{
		return ele%wielkosc;
	}

	void insert(int ele)
	{
		tab[kompresja(ele)].add_back(ele);
	}

	void remove(int ele)
	{
		for (int i = 0;i < tab[kompresja(ele)].get_size();i++)
			if (tab[kompresja(ele)][i] == ele)
				tab[kompresja(ele)].del(i);
	}

	void search(int ele)
	{
		for (int i = 0;i < tab[kompresja(ele)].get_size();i++)
			if (tab[kompresja(ele)][i] == ele)
				cout << "znaleziono element o indeksie tablicy: "<< kompresja(ele) <<" pozycja w liscie nr: " << i <<endl;
	}

	void print()
	{
		for (int i = 0;i < wielkosc;i++)
			tab[i].print();
	}

	bool pierwsza(int n)
	{
		if (n<2)
			return false;
		for (int i = 2;i*i <= n;i++)
			if (n%i == 0)
				return false;
		return true;
	}

};

#endif 