#pragma once

#ifndef _WIESZCHOLEK_  
#define _WIESZCHOLEK_

#include <iostream>
#include <string>
#include "list.h"
//#include "krawedz.h"

using namespace std;


template<typename E,typename K>
class wieszcholek
{

private:
	Deque <K*> sasiectwo;
	E wartosc;
public:
	wieszcholek(E x = 1) : wartosc(x)
	{}

	E get_wartosc()
	{
		return wartosc;
	}

	void set_wartosc(E x)
	{
		wartosc=x;
	}

	K* get_krawedz(int i)
	{
		return sasiectwo[i];
	}

	Deque <K*>* get_sasiectwo()
	{
		return &sasiectwo;
	}

	void add_krawedz(K* k)
	{
		sasiectwo.add_back(k);
	}
	
	K remove_krawedz(int i)
	{
		return sasiectwo.remove(i)
	}

};

#endif 