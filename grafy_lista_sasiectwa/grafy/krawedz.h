#pragma once

#ifndef _KRAWEDZ_  
#define _KRAWEDZ_  

#include <iostream>
#include <string>

#include "wieszcholek.h"
//using namespace std;


template<typename E>//, typename K>
class krawedz
{
private:
	E wartosc;
	wieszcholek<E, krawedz<E>>* end[2];
	//wieszcholek<E, krawedz<E>>* drugi;
public:


	krawedz(wieszcholek<E, krawedz<E>>* a=nullptr, wieszcholek<E, krawedz<E>>* b=nullptr, E w=1)
	{
		end[0] = a;
		end[1] = b;
		wartosc = w;
	}

	~krawedz() = default;
	
	void set_wartosc(E ele)
	{
		wartosc = ele;
	}

	E get_wartosc()
	{
		return wartosc;
	}

	void set_jeden(wieszcholek<E, krawedz<E>> *j)
	{
		end[0] = j;
	}

	wieszcholek<E, krawedz<E>> * get_jeden()
	{
		return end[0];
	}

	void set_drugi(wieszcholek<E, krawedz<E>> *d)
	{
		end[1] = d;
	}

	wieszcholek<E, krawedz<E>>* get_drugi()
	{
		return end[1];
	}

	wieszcholek<E, krawedz<E>>** get_tab()
	{
		return end;
	}
};

#endif 