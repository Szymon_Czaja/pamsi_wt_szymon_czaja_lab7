#pragma once
#pragma once
#ifndef _LISTA_   
#define _LISTA_

#include <iostream>
#include <string>

using namespace std;

template<typename E>
class wezel
{
private:
	E element;
	wezel<E>* next;
	wezel<E>* back;
	int key;
public:
	wezel()
	{
		next = NULL;
		back = NULL;
		key = 999;
	}

	E get_element()
	{
		return element;
	}

	void set_element(E elem)
	{
		element = elem;
	}

	wezel<E>* get_next()
	{
		return next;
	}

	void set_next(wezel<E>* nastepny)
	{
		next = nastepny;
	}

	wezel<E>* get_back()
	{
		return back;
	}

	void set_back(wezel<E>* poprzedni)
	{
		back = poprzedni;
	}

	int get_key()
	{
		return key;
	}

	void set_key(int i)
	{
		key = i;
	}
};

template<typename E>
class Deque
{
private:
	wezel<E>* head;
	wezel<E>* tail;
	int size;
public:
	Deque()
	{
		head = NULL;
		tail = NULL;
		size = 0;
	}

	~Deque()
	{
		this->delete_all();
	}

	bool empty()
	{
		if (head == NULL)
			return true;
		else
			return false;
	}

	wezel<E>* get_head()
	{
		return head;
	}

	void add_front(const E& dana)
	{
		wezel<E>* tmp = head;
		head = new wezel <E>;
		head->set_element(dana);
		head->set_next(tmp);
		head->set_back(NULL);
		if (tail == NULL)
			tail = head;
		else
			head->get_next()->set_back(head);
		size++;
	}

	void add_back(const E& dana)
	{
		wezel<E>* tmp = tail;
		tail = new wezel <E>;
		tail->set_element(dana);
		tail->set_next(NULL);
		tail->set_back(tmp);
		if (head == NULL)
			head = tail;
		else
			tail->get_back()->set_next(tail);
		size++;
	}

	E front()
	{
		E dana;
		if (head == NULL)
		{
			string wyjatek = "front ()pusta  lista !";
			throw wyjatek;
		}
		else if (this->get_size() == 1)
		{
			wezel<E>* tmp = head;
			dana = head->get_element();
			head = NULL;
			tail = NULL;
			delete tmp;
			size--;
			return dana;
		}
		else
		{
			wezel<E>* tmp = head;
			dana = head->get_element();
			head = head->get_next();
			head->set_back(NULL);
			delete tmp;
			size--;
			return dana;
		}
	}

	E back()
	{
		E dana;
		if (tail == NULL)
		{
			string wyjatek = " back() pusta  lista !";
			throw wyjatek;
		}
		else if (this->get_size() == 1)
		{
			wezel<E>* tmp = tail;
			dana = tail->get_element();
			tail = tail->get_back();
			head = tail;
			delete tmp;
			size--;
			return dana;
		}
		else
		{
			wezel<E>* tmp = tail;
			dana = tail->get_element();
			tail = tail->get_back();
			tail->set_next(NULL);
			delete tmp;
			size--;
			return dana;
		}
	}

	int get_size()
	{
		return size;
	}

	void print()
	{
		wezel<E>* tmp = head;
		while (tmp != NULL)
		{
			cout << tmp->get_element() << endl;
			tmp = tmp->get_next();
		}
	}

	void delete_all()
	{
		while (head != NULL)
			this->front();
	}

	//========================================================================

	E operator [](int const i)
	{
		int j;
		wezel<E>* tmp;
		E cos;
		if (i >= this->get_size())
		{
			string wyjatek = " [] przekroczono zakres !";
			throw wyjatek;
		}
		else if (i < this->get_size() / 2)
		{
			j = 0;
			tmp = head;
			while (i != j)
			{
				j++;
				tmp = tmp->get_next();
			}
			cos = tmp->get_element();
			return cos;
		}
		else
		{
			j = (this->get_size() - 1);
			tmp = tail;
			while (i != j)
			{
				j--;
				tmp = tmp->get_back();
			}
			cos = tmp->get_element();
			return cos;
		}

	}

	void del(int i)
	{
		int j = 0;
		wezel<E>* tmp;
		if (i >= this->get_size())
			cout << " del ()nie ma takiego elementu, przekoczno zakres listy nie mozna usunac" << endl;
		else if (i == 0)
		{
			tmp = head->get_next();
			if(get_size()!=1)
				head->get_next()->set_back(NULL);
			delete head;
			head = tmp;
			size--;
		}
		else if (i == this->get_size() - 1)
		{
			tmp = tail->get_back();
			if (get_size() != 1)
				tail->get_back()->set_next(NULL);
			delete tail;
			tail = tmp;
			size--;
		}
		else
		{
			tmp = head;
			while (i != j)
			{
				j++;
				tmp = tmp->get_next();
			}
			tmp->get_next()->set_back(tmp->get_back());
			tmp->get_back()->set_next(tmp->get_next());
			delete tmp;
			size--;
		}
	}

	int add_after(int i, E ele)
	{
		int j = 0;
		wezel<E>* tmp;
		if (i >= get_size() || i < -1)
		{
			string wyjatek = " add_after() zly indeks !";
			throw wyjatek;
		}
		else if (i == -1)
		{
			add_front(ele);
			return i + 1;
		}
		else if (i == get_size() - 1)
		{
			add_back(ele);
			return i + 1;
		}
		else
		{
			tmp = head;
			while (i != j)
			{
				j++;
				tmp = tmp->get_next();
			}
			tmp->get_next()->set_back(new wezel<E>);					// -> natepny -> poprzedni-> nowy
			tmp->get_next()->get_back()->set_next(tmp->get_next());		// nowy ustawienie nastepnego 
			tmp->get_next()->get_back()->set_back(tmp);					// nowy ustawienie poprzedniego
			tmp->set_next(tmp->get_next()->get_back());					//->nastepny=nowy
			tmp->get_next()->set_element(ele);							//ustawianie elementu
			size++;
			return i + 1;
		}
	}

	E remove(int i)
	{
		int j = 0;
		wezel<E>* tmp;
		E ele;
		if (i >= get_size() || i < 0)
		{
			string wyjatek = " remove() zly indeks !";
			throw wyjatek;
		}
		else if (i == 0)
		{
			ele = front();
			return ele;
		}
		else if (i == get_size() - 1)
		{
			ele = back();
			return ele;
		}
		else
		{
			tmp = head;
			while (i != j)
			{
				j++;
				tmp = tmp->get_next();
			}
			tmp->get_next()->set_back(tmp->get_back());
			tmp->get_back()->set_next(tmp->get_next());
			ele = tmp->get_element();
			delete tmp;
			size--;
			return ele;
		}
	}

	int index_of(E ele)
	{
		int i = 0;
		wezel<E>* tmp = head;
		while (tmp != NULL)
		{
			if (tmp->get_element() == ele)
				return i;
			i++;
			tmp = tmp->get_next();
		}
		string wyjatek = " index_of() brak elementu pasujacego!";
		throw wyjatek;
	}

	bool jest(E ele)
	{
		wezel<E>* tmp = head;
		while (tmp != NULL)
		{
			if (tmp->get_element() == ele)
				return true;
			tmp = tmp->get_next();
		}
		return false;
	}

};
#endif