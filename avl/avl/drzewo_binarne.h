#pragma once

#ifndef _DRZEWO_BINARNE_  
#define _DRZEWO_BINARNE_    

#include "wezel.h"
#include <iostream>


using namespace std;

template<typename E>
class drzewo_binarne
{
private:
	wezel<E>* head;
	int s;
public:


	drzewo_binarne()
	{
		head = new wezel<E>;
		s = 0;
		head->set_element(NULL);
	}

	drzewo_binarne(E ele)
	{
		head = new wezel<E>;
		s = 1;
		head->set_element(ele);
	}

	~drzewo_binarne()
	{
		remove_this(head);
	}

	void set_size(int i)
	{
		s = i;
	}

	void set_head(wezel<E>* nowy)
	{
		head = nowy;
	}
	
	bool empty()
	{
		if (head->get_sons_count() == 0)
			return true;
		else
			return false;
	}

	wezel<E>* get_head()
	{
		return head;
	}

	wezel<E>* add_son_P(const E dana, wezel<E>* ojciec)
	{
		if (ojciec->get_next_P() != NULL)
		{
			string wyjatek = "drzewo add_son_P() nie mozna utworzyc, wezel juz zainicjowany, zwolnij miejsce ";
			throw wyjatek;
		}
		else
		{
			ojciec->set_next_P(new wezel <E>);
			ojciec->get_next_P()->set_element(dana);
			ojciec->get_next_P()->set_back(ojciec);
			s++;
			return ojciec->get_next_P();
		}
	}

	wezel<E>* add_son_L(const E dana, wezel<E>* ojciec)
	{
		if (ojciec->get_next_L() != NULL)
		{
			string wyjatek = "drzewo add_son_L() nie mozna utworzyc, wezel juz zainicjowany, zwolnij miejsce ";
			throw wyjatek;
		}
		else
		{
			ojciec->set_next_L(new wezel <E>);
			ojciec->get_next_L()->set_element(dana);
			ojciec->get_next_L()->set_back(ojciec);
			s++;
			return ojciec->get_next_L();
		}
	}

	void remove_this(wezel<E>* ojciec)
	{
		if (ojciec->get_next_P() == NULL && ojciec->get_next_L() == NULL)
		{
			delete ojciec;
			s--;
		}
		else
		{
			if (ojciec->get_next_P() != NULL)
			{
				remove_this(ojciec->get_next_P());
				ojciec->set_next_P(NULL);
			}
			if (ojciec->get_next_L() != NULL)
			{
				remove_this(ojciec->get_next_L());
				ojciec->set_next_L(NULL);
			}
			delete ojciec;
			s--;
		}
	}

	E remove_son_P(wezel<E>* ojciec)
	{
		if (ojciec->get_next_P() == NULL)
		{
			string wyjatek = "drzewo remove_son_P() nie mozna usunac, nie zainicjowany wezel";
			throw wyjatek;
		}
		else
		{
			E tmp = ojciec->get_next_P()->get_element();
			remove_this(ojciec->get_next_P());
			ojciec->set_next_P(NULL);
			return tmp;
		}
	}

	E remove_son_L(wezel<E>* ojciec)
	{
		if (ojciec->get_next_L() == NULL)
		{
			string wyjatek = "drzewo remove_son_L() nie mozna usunac, nie zainicjowany wezel";
			throw wyjatek;
		}
		else
		{
			E tmp = ojciec->get_next_L()->get_element();
			remove_this(ojciec->get_next_L());
			ojciec->set_next_L(NULL);
			return tmp;
		}
	}

	int size()
	{
		return s;
	}
	//================================================PRINT====================================================================

	void print_post(wezel<E>* ojciec)
	{
		if ((ojciec->get_next_P() == NULL && ojciec->get_next_L() == NULL))
			cout << ojciec->get_element() << endl;
		else
		{
			if (ojciec->get_next_L() != NULL)
				print_post(ojciec->get_next_L());

			if (ojciec->get_next_P() != NULL)
				print_post(ojciec->get_next_P());

			cout << ojciec->get_element() << endl;
		}
	}

	void print_post()
	{
		wezel<E>* ojciec = get_head();
		if ((ojciec->get_next_P() == NULL && ojciec->get_next_L() == NULL))
			cout << ojciec->get_element() << endl;
		else
		{
			if (ojciec->get_next_L() != NULL)
				print_post(ojciec->get_next_L());

			if (ojciec->get_next_P() != NULL)
				print_post(ojciec->get_next_P());

			cout << ojciec->get_element() << endl;
		}
	}

	void print_pre(wezel<E>* ojciec)
	{
		if ((ojciec->get_next_P() == NULL && ojciec->get_next_L() == NULL))
			cout << ojciec->get_element() << endl;
		else
		{
			cout << ojciec->get_element() << endl;

			if (ojciec->get_next_L() != NULL)
				print_pre(ojciec->get_next_L());

			if (ojciec->get_next_P() != NULL)
				print_pre(ojciec->get_next_P());
		}
	}

	void print_pre()
	{
		wezel<E>* ojciec = get_head();
		if ((ojciec->get_next_P() == NULL && ojciec->get_next_L() == NULL))
			cout << ojciec->get_element() << endl;
		else
		{
			cout << ojciec->get_element() << endl;

			if (ojciec->get_next_L() != NULL)
				print_pre(ojciec->get_next_L());

			if (ojciec->get_next_P() != NULL)
				print_pre(ojciec->get_next_P());
		}
	}

	void print_in(wezel<E>* ojciec)
	{
		if ((ojciec->get_next_P() == NULL && ojciec->get_next_L() == NULL))
			cout << ojciec->get_element() << endl;
		else
		{
			if (ojciec->get_next_L() != NULL)
				print_in(ojciec->get_next_L());

			cout << ojciec->get_element() << endl;

			if (ojciec->get_next_P() != NULL)
				print_in(ojciec->get_next_P());
		}
	}

	void print_in()
	{
		wezel<E>* ojciec = get_head();
		if ((ojciec->get_next_P() == NULL && ojciec->get_next_L() == NULL))
			cout << ojciec->get_element() << endl;
		else
		{
			if (ojciec->get_next_L() != NULL)
				print_in(ojciec->get_next_L());

			cout << ojciec->get_element() << endl;

			if (ojciec->get_next_P() != NULL)
				print_in(ojciec->get_next_P());
		}
	}

	int height(wezel<E> *ojciec)
	{
		if(ojciec==NULL)
			return 0;
		else if (ojciec->get_next_L() == NULL && ojciec->get_next_P() == NULL)
			return 1;
		else
		{
			int h = 1;
			if (ojciec->get_next_P() != NULL)
			{
				int k = height(ojciec->get_next_P());
				if (h < k)
					h = k;
			}
			if (ojciec->get_next_L() != NULL)
			{
				int k = height(ojciec->get_next_L());
				if (h < k)
					h = k;
			}
			return h + 1;
		}
	}
};


#endif 