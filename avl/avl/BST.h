#pragma once

#ifndef _BST_  
#define _BST_    

#include "drzewo_binarne.h"


template<typename E>
class BST 
	: public drzewo_binarne<E>
{
public:

	wezel<E>* search(E ele, wezel<E>* root) //zwraca wezel nalblizej elementu - jak ele nie istnieje to zwraca ojca tam gdzie go wlozyc
	{
		if (root == NULL)
		{
			string wyjatek = "bst search() nie mozna odnalezc, nie zainicjowany wezel root";
			throw wyjatek;
		}

		if (ele < root->get_element())
		{
			if (root->get_next_L() == NULL)
				return root;
			else
				return search(ele, root->get_next_L());
		}

		else if (ele == root->get_element())
			return root;

		else 
		{
			if(root->get_next_P() == NULL)
				return root;
			else
				return search(ele, root->get_next_P());
		}
	}

	wezel<E>* search_min(wezel<E>* root)
	{
		if (root == NULL)
		{
			string wyjatek = "bst search_min() nie mozna odnalezc, nie zainicjowany wezel root";
			throw wyjatek;
		}

		if (root->get_next_L() == NULL)
			return root;
		else
			return search_min(root->get_next_L());
	}

	wezel<E>* search_max(wezel<E>* root)
	{
		if (root == NULL)
		{
			string wyjatek = "bst search_max() nie mozna odnalezc, nie zainicjowany wezel root";
			throw wyjatek;
		}

		if (root->get_next_P() == NULL)
			return root;
		else
			return search_max(root->get_next_P());
	}

	wezel<E>* search_next(wezel<E>* root)
	{
		if (root == NULL)
		{
			string wyjatek = "bst search_next() nie mozna odnalezc, nie zainicjowany wezel root";
			throw wyjatek;
		}

		if (root->get_next_P() != NULL)
			return search_min(root->get_next_P());
		else
		{
			wezel<E>* tmp = root;
			while (tmp->get_back() != NULL)
			{
				if (tmp->get_element() >= root->get_element())
					return tmp;
				tmp = tmp->get_back();
			}
			return NULL;
		}
	}

	wezel<E>* search_back(wezel<E>* root)
	{
		if (root == NULL)
		{
			string wyjatek = "bst search_next() nie mozna odnalezc, nie zainicjowany wezel root";
			throw wyjatek;
		}

		if (root->get_next_L() != NULL)
			return search_max(root->get_next_L());
		else
		{
			wezel<E>* tmp = root;
			while (tmp->get_back() != NULL)
			{
				if (tmp->get_element() <= root->get_element())
					return tmp;
				tmp = tmp->get_back();
			}
			return NULL;
		}
	}

	void insert(wezel<E>* ojciec,E ele)
	{
		if (ele > ojciec->get_element())
			{
			if (ojciec->get_next_P() == NULL)
				add_son_P(ele, ojciec);
			else
				insert(ojciec->get_next_P(),ele);
			}
			else
			{
			if (ojciec->get_next_L() == NULL)
				add_son_L(ele, ojciec);
			else
				insert(ojciec->get_next_L(),ele);
			}
		}

	virtual void insert(E ele)
	{
		if (size() == 0)
		{
			get_head()->set_element(ele);
			set_size(1);
		}
		else 
		{
			wezel<E>* ojciec = search(ele, get_head());
			insert(ojciec,ele);
		}
	}

	void remove(wezel<E>* ojciec)
	{
		if (ojciec == get_head()||(ojciec->get_next_L() != NULL && ojciec->get_next_P() != NULL))
		{
			wezel<E>* nastepnik = search_next(ojciec);
			wezel<E>* poprzednik = search_back(ojciec);
			if (nastepnik != NULL)
			{
				ojciec->set_element(nastepnik->get_element());
				remove(nastepnik);
			}
			else if (poprzednik != NULL)
			{
				ojciec->set_element(poprzednik->get_element());
				remove(poprzednik);
			}
			else
				cout << "bst remove() nie mozna usunac jedynego wezla!\n";
		}

		if (ojciec->get_next_L() == NULL && ojciec->get_next_P() == NULL)  //jest lisciem
		{
			if (ojciec->get_back()->get_next_L() == ojciec)				//ojciec jest lewym synem
				ojciec->get_back()->set_next_L(NULL);
			if (ojciec->get_back()->get_next_P() == ojciec)				//ojciec jest prawym synem
				ojciec->get_back()->set_next_P(NULL);
			delete ojciec;
			set_size(size() - 1);
		}
		else if (ojciec->get_next_L() != NULL && ojciec->get_next_P() == NULL) //ma lewego syna tylko
		{
			if (ojciec->get_back()->get_next_L() == ojciec)				//ojciec jest lewym synem
				ojciec->get_back()->set_next_L(ojciec->get_next_L());
			if (ojciec->get_back()->get_next_P() == ojciec)				//ojciec jest prawym synem
				ojciec->get_back()->set_next_P(ojciec->get_next_L());
			ojciec->get_next_L()->set_back(ojciec->get_back());
			delete ojciec;
			set_size(size() - 1);
		}
		else if (ojciec->get_next_L() == NULL && ojciec->get_next_P() != NULL)//ma prawego syna tylko
		{
			if (ojciec->get_back()->get_next_L() == ojciec)				//ojciec jest lewym synem
				ojciec->get_back()->set_next_L(ojciec->get_next_P());
			if (ojciec->get_back()->get_next_P() == ojciec)				//ojciec jest prawym synem
				ojciec->get_back()->set_next_P(ojciec->get_next_P());
			ojciec->get_next_P()->set_back(ojciec->get_back());
			delete ojciec;
			set_size(size() - 1);
		}
	}
	
	virtual void remove(E ele)
	{
		wezel<E>* ojciec = search(ele, get_head());
		if (ojciec->get_element() != ele)
			cout << "bst remove() nie mozna odnalezc, nie mozna usunac!\n";
		else
			remove(ojciec);
	}

};


#endif 