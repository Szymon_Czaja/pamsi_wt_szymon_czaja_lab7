#pragma once

#ifndef _AVL_  
#define _AVL_    

#include "BST.h"


template<typename E>
class AVL
	: public BST<E>
{
public:
	void restructure(wezel<E>* ojciec)
	{
	
		if (ojciec->get_bf() == 2)
		{
			if (ojciec->get_next_L()->get_next_L() != NULL)
				LL(ojciec);
			else
				LR(ojciec);
		}
		else if (ojciec->get_bf() == -2)
		{
			if (ojciec->get_next_P()->get_next_P()!=NULL)
				RR(ojciec);
			else
				RL(ojciec);
		}
		else if (ojciec->get_back() == NULL)
			return;
		restructure(ojciec->get_back());

	}

	void RR(wezel<E>* A)
	{
		wezel<E>* B = A->get_next_P();
		wezel<E>* O = A->get_back();
		if (A->get_back() != NULL)
		{
			if (A->get_back()->get_next_L() == A)				//ojciec jest lewym synem
				A->get_back()->set_next_L(B);
			if (A->get_back()->get_next_P() == A)				//ojciec jest prawym synem
				A->get_back()->set_next_P(B);
		}
		else
			set_head(B);
		A->set_next_P(B->get_next_L());
		if (A->get_next_P() != NULL)
			A->get_next_P()->set_back(A);
		B->set_back(O);
		B->set_next_L(A);
		A->set_back(B);
		if (B->get_bf() == -1) {
			A->set_bf_w(0);
			B->set_bf_w(0);
		}
		else {
			A->set_bf_w(-1);
			B->set_bf_w(1);
		}
	
	}

	void LL(wezel<E>* A)
	{
		wezel<E>* B = A->get_next_L();
		wezel<E>* O = A->get_back();
		if (A->get_back() != NULL)
		{
			if (A->get_back()->get_next_L() == A)				//ojciec jest lewym synem
				A->get_back()->set_next_L(B);
			if (A->get_back()->get_next_P() == A)				//ojciec jest prawym synem
				A->get_back()->set_next_P(B);
		}
		else
			set_head(B);
		A->set_next_L(B->get_next_P());
		if (A->get_next_L() != NULL)
			A->get_next_L()->set_back(A);
		B->set_back(O);
		B->set_next_P(A);
		A->set_back(B);
		if (B->get_bf() == 1) {
			A->set_bf_w(0);
			B->set_bf_w(0);
		}
		else {
			A->set_bf_w(1);
			B->set_bf_w(-1);
		}
	}

	void RL(wezel<E>* A)
	{
		LL(A->get_next_P());
		RR(A);
	}

	void LR(wezel<E>* A)
	{
		RR(A->get_next_L());
		LL(A);
	}

	void set_bf(wezel<E>* ojciec)
	{
		wezel<E>* tmp = ojciec;
		while (tmp != NULL)
		{
			tmp->set_bf_w(height(tmp->get_next_L()) - height(tmp->get_next_P()));
			tmp = tmp->get_back();
		}
	}

	void insert(E ele)
	{
		if (size() == 0)
		{
			get_head()->set_element(ele);
			set_size(1);
			set_bf(get_head());
		}
		else
		{
			wezel<E>* ojciec = search(ele, get_head());
			BST::insert(ojciec, ele);
			set_bf(ojciec);
			restructure(ojciec);
		}
	}

	void remove(E ele)
	{
		{
			wezel<E>* ojciec = search(ele, get_head());
			wezel<E>* dziadek = ojciec->get_back();
			if (ojciec->get_element() != ele)
				cout << "bst remove() nie mozna odnalezc, nie mozna usunac!\n";
			else
				BST::remove(ojciec);
			set_bf(dziadek);
			restructure(dziadek);
		}
	}

	void print(wezel<E>* ojciec)
	{
		set_bf_all(ojciec);
		if ((ojciec->get_next_P() == NULL && ojciec->get_next_L() == NULL))
			cout << "wysokosc: " << height(ojciec) << " bf: " << ojciec->get_bf() << " element: " << ojciec->get_element() << endl;
		else
		{
			if (ojciec->get_next_L() != NULL)
				print(ojciec->get_next_L());

			cout << "wysokosc: "<<height(ojciec)<< " bf: " << ojciec->get_bf() << " element: " << ojciec->get_element() << endl;

			if (ojciec->get_next_P() != NULL)
				print(ojciec->get_next_P());
		}
	}

	void set_bf_all(wezel<E>* ojciec)
	{
		if ((ojciec->get_next_P() == NULL && ojciec->get_next_L() == NULL))
			ojciec->set_bf_w(0);
		else
		{
			if (ojciec->get_next_L() != NULL)
				set_bf(ojciec->get_next_L());

			ojciec->set_bf_w(height(ojciec->get_next_L()) - height(ojciec->get_next_P()));

			if (ojciec->get_next_P() != NULL)
				set_bf(ojciec->get_next_P());
			set_bf(ojciec);
		}
	}
};


#endif 